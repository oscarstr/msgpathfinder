package de.ostr;

import de.ostr.util.Utils;
import de.ostr.model.Distance;
import de.ostr.model.Path;
import org.apache.lucene.spatial.util.GeoDistanceUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class PathFinder {

    private static final double DISTANCE_TOO_FAR = 10000.0D;

    private Map<Long, Map<Long, Double>> distances = new HashMap<>();

    private Map<List<Long>, Path> pathCache = new HashMap<>();

    private List<Long> allRequiredNodes;

    private long startingId;

    private static final String CSV_SEPERATOR = ",";

    public static void main(String[] args) throws IOException, URISyntaxException {
        PathFinder pathFinder = new PathFinder();
        pathFinder.execute(args.length > 0 ? args[0] : "msg_standorte_deutschland.csv", args.length > 1? Long.parseLong(args[1]) : 1l);
    }


    public void execute(String fileName, Long startingId) throws IOException, URISyntaxException {
        this.startingId = startingId;
        LocalDateTime before = LocalDateTime.now();
        init(fileName);
        Path path = getTotalDistance(startingId, allRequiredNodes);
        System.out.println("Shortest Path:" + path.getPath().stream().map(l -> l.toString()).collect(Collectors.joining("->")) + "->"+ startingId+ ": " + path.getDistance()+ "km");
        LocalDateTime after = LocalDateTime.now();
        System.out.println(before.until(after, ChronoUnit.MILLIS));
    }

    private void init(String fileName) throws IOException, URISyntaxException {
        List<String[]> csvLines = getCsvLines(fileName);
        readRequiredNodes(csvLines);
        Map<Long, List<Distance>> realisticDistanceMap = calculateDistancesFromCsvFile(csvLines);
        addToPerformantDistancesMap(realisticDistanceMap);
    }

    /**
     * reads the given file and returns a list of the values of each line
     * @param fileName
     * @return
     * @throws IOException
     */
    private List<String[]> getCsvLines(String fileName) throws IOException {
        return Files.lines(Paths.get(fileName)).map(s -> s.split(CSV_SEPERATOR)).collect(Collectors.toList());
    }

    /**
     * reads all node ids except the start id from the given fileName
     * @param csvLines
     * @throws IOException
     */
    private void readRequiredNodes(List<String[]> csvLines) throws IOException {

        allRequiredNodes = csvLines.stream().map(csvLine -> Long.parseLong(csvLine[0])).filter(id -> !id.equals(startingId)).collect(Collectors.toList());
    }

    /**
     * Creates a map, that contains a key for each location and a list that includes the Distances to each other location as value
     * @param csvLines
     * @return
     * @throws IOException
     * @throws URISyntaxException
     */
    private Map<Long, List<Distance>> calculateDistancesFromCsvFile(List<String[]> csvLines) throws IOException, URISyntaxException {
        Map<Long, List<Distance>> realisticDistanceMap = new HashMap<>();
        csvLines.stream().forEach(csvLineSource -> csvLines.forEach(csvLineTarget -> addDistanceToMap(realisticDistanceMap, csvLineSource, csvLineTarget)));
        return realisticDistanceMap;
    }

    /**
     * calculates the distance between the longitude / latitude fields of the given csv lines, converts them to KM and adds them to the given map
     * @param map
     * @param csvLineSource
     * @param csvLineTarget
     */
    private void addDistanceToMap(Map<Long, List<Distance>> map, String[] csvLineSource, String[] csvLineTarget){
        List<Distance> distances = map.computeIfAbsent(Long.parseLong(csvLineSource[0]), x -> new ArrayList());
        double lonA = Double.parseDouble(csvLineSource[7]);
        double latA = Double.parseDouble(csvLineSource[6]);
        double lonB = Double.parseDouble(csvLineTarget[7]);
        double latB = Double.parseDouble(csvLineTarget[6]);
        double distanceInKm = GeoDistanceUtils.vincentyDistance(lonA, latA, lonB, latB) / 1000;
        distances.add(new Distance(Long.parseLong(csvLineTarget[0]), distanceInKm));
    }

    /**
     * Adds the map to performantDistances, so that distances can be found by multilayered keys instead of filter the list of Distances
     * @param realisticDistanceMap
     */
    private void addToPerformantDistancesMap(Map<Long, List<Distance>> realisticDistanceMap) {
        realisticDistanceMap.values().stream().forEach(this::filterUnrealisticNodes);
        realisticDistanceMap.entrySet().stream().forEach(entry -> putToPerformantDistances(entry.getKey(), entry.getValue()));
    }


    /**
     * Removes all distances, that are more than 10% bigger than the 3rd closest distance, because it's extremely unlikely that distances like that have to be traveled for the shortest total distance.
     * Also, the list will be sorted.
     * @param distances
     */
    private void filterUnrealisticNodes(List<Distance> distances) {
        sortList(distances);
        Double threshold = distances.get(3).getDistance() * 1.1;
        distances.removeIf(distance -> distance.getDistance()> threshold);
    }

    /**
     * sorts the given list ascending
     * @param list
     */
    private void sortList(List<Distance> list) {
        list.sort((x, y)-> (int)(x.getDistance() -y.getDistance()));
        list.remove(0);
    }

    /**
     * Transforms the given List of distances to a key/value pair of "to" and "value" and adds them to the Map, that is identified by from.. If from does not have a to map yet, it will be created
     * @param from
     * @param distances
     */
    private void putToPerformantDistances(Long from, List<Distance> distances){
        Map<Long, Double> targetMap = this.distances.computeIfAbsent(from, x -> new HashMap<>());
        distances.forEach(distance -> targetMap.put(distance.getTo(), distance.getDistance()));
    }


    /**
     * Gets the total minimum remaining distance for the given nodeId and remainingNodes. If the distance is not stored in the pathCache yet, it's calculated using findShortestPath
     * @param nodeId id of the path that should be determined
     * @param remainingNodes the nodes that are still missing on the total path
     */
    private Path getTotalDistance(Long nodeId, List<Long> remainingNodes){

        Map<Long, Double> currentDistances = this.distances.get(nodeId);

        if (remainingNodes.isEmpty()){
            return new Path(Arrays.asList(nodeId), getTotalDistance(nodeId, startingId));
        }
        ArrayList<Long> pathIdentifier = new ArrayList<>(remainingNodes);
        pathIdentifier.add(0, nodeId);

        return pathCache.computeIfAbsent(pathIdentifier, (x) -> findShortestPath(nodeId, remainingNodes, currentDistances));
    }

    /**
     *
     * @param idFrom
     * @param idTo
     * @return the distance between idFrom and idTo by looking up in performantDistances.
     * @throws RuntimeException if distance cannot be found
     */
    private double getTotalDistance(Long idFrom, Long idTo) {
        return Optional.ofNullable(distances
                .get(idFrom)
                .get(idTo)).orElseThrow(() -> new RuntimeException("Couldn't find distance"));
    }

    /**
     * compares the possible paths using getTotalDistance and returns the shortest. If no path could be found, because they're not part of the performantDistances Map, DISTANCE_TOO_FAR will be returned instead of actually looking for a way
     * @param nodeId
     * @param remainingNodes
     * @param currentDistances
     * @return
     */
    private Path findShortestPath(Long nodeId, List<Long> remainingNodes, Map<Long, Double> currentDistances) {
        try {
            return currentDistances.entrySet().stream()
                    .map(Map.Entry::getKey)
                    .filter(remainingNodes::contains)
                    .map(node -> getTotalDistance(node, Utils.copyListWithout(remainingNodes, node)))
                    .map(path -> addNodeToPath(nodeId, path))
                    .sorted((entryA, entryB) -> (int)(entryA.getDistance()-entryB.getDistance()))
                    .findFirst()
                    .orElseThrow(() -> new RuntimeException("Couldn't find any path"));
        } catch (RuntimeException e){
            ArrayList<Long> newNodes = new ArrayList<>(remainingNodes);
            newNodes.add(0, nodeId);
            return new Path(newNodes, DISTANCE_TOO_FAR);
        }
    }

    /**
     * Creates a new Path Object, with the given nodeId as first entry of the Paths NodeList and the distance of the nodeId to the first element of the old NodeList added to the old distance
     * @param nodeId
     * @param path
     * @return
     */
    private Path addNodeToPath(Long nodeId, Path path) {
        ArrayList arrayList = new ArrayList(path.getPath());
        arrayList.add(0, nodeId);
        return new Path(arrayList, path.getDistance() + getTotalDistance(nodeId, path.getPath().get(0)));
    }



}
