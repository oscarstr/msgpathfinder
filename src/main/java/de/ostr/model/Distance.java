package de.ostr.model;

public class Distance {
    Long to;
    Double distance;

    public Distance(Long to, Double distance) {
        this.to = to;
        this.distance = distance;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
}
