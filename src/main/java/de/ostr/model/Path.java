package de.ostr.model;

import java.util.List;

public class Path {
    private List<Long> path;
    private Double distance;

    public Path(List<Long> path, Double distance) {
        this.path = path;
        this.distance = distance;
    }

    public List<Long> getPath() {
        return path;
    }

    public void setPath(List<Long> path) {
        this.path = path;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

}
