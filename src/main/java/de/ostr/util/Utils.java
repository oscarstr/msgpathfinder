package de.ostr.util;

import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static <T> List<T> copyListWithout(List<T> list, T objectToBeRemoved) {
        ArrayList<T> listCopy = new ArrayList<>(list);
        listCopy.remove(objectToBeRemoved);
        return listCopy;
    }
}
